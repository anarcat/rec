/****************************-*-C-*-***********************************
 * $Id: display.c,v 1.6 2001/08/23 01:27:01 anarcat Exp anarcat $
 **********************************************************************
 * Display procedures declarations for rec
 **********************************************************************
 *   Copyright (C) 2001 The Anarcat <anarcat@anarcat.dyndns.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <stdio.h>

#include "display.h"
#include "convert.h"

float diff = 0.0;
long int real_toread = 0,
  now_read = 0,
  time = 0,
  eta = 0;

__inline__ void update_display (long int readcount, float d) {
  now_read += readcount;
  diff += d;/* TIME_FROM_BYTES((float) readcount, params->fmt, */
/*                           params->channels, params->rate); */
  if (diff >= 1.0) {
    eta--;
    time++;
    diff--;
    refresh_display();
  }
}

/* update the display status line, pass real_toread = 0 to avoid eta */
void display (long int n,       /* what we have read now */
              long int r,       /* how much data we have to read at all */
              int t,            /* time passed, in seconds */
              int e) {          /* time left (ETA), in seconds */
  if (r && e) {
    fprintf(stderr,
            "\r%10li/%-10li %d:%02d:%02d/%d:%02d:%02d",
            n, r,
            (int) t/3600, (int) (t%3600)/60, (int) t % 60, 
            (int) e/3600, (int) (e%3600)/60, (int) e % 60);
  } else {
    fprintf(stderr,
            "\r%10li %d:%02d:%02d", n,
            (int) t/3600, (int) (t%3600)/60, (int) t % 60);
  }
}

__inline__ void refresh_display () {
  display(now_read, real_toread, time, eta);
}
