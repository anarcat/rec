/* dB = log10(level)*20.0. */

/*
Opps.  That formula I gave above is wrong.  It should really be:
     
     float dB = log10((double)SHRT_MAX / max) * 20.0;
     
The extra factor of two is necessary because dB is log10(x^2) and
pulling the square outside of the log10 is more efficient
computationally.

In addition, sometimes you will see systems showing negative dB.
This comes from inverting the SHRT_MAX/max component or equivalently
multiplying log10 by -1.  dB can also be shown relative to other base
values which can effectively add or subtract constants when converting
between dB levels.

You might also want to use the difference between min and max to get
the full power range.  However, if the signal is offset from zero it
will be the the actual max or min which causes clipping.

log10( (2^16)^2 ) * 10 = log10(2^16) * 20 = 96.3 which is where the 96
dB range for 16 bit sound cards comes from.

log10(2^1) * 20 = 6.02 dB per bit.
*/
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <signal.h>

#include "config.h"

unsigned char* buffer;

void usage (char* argv0) {

  fprintf(stderr,
          "\
usage: %s [file]\n", argv0);
}

__inline__ double c_db (u_int16_t val) {

  return log10((double)UINT_MAX / val) * 10.0;

}

void pr () {

  double dB = c_db((u_int16_t) buffer[0]);

  fprintf(stderr, "\r%10.4f", dB);
  alarm(1);

}

int main (int argc, char** argv) {

  char ch;
  int sndFile;

  while ((ch = getopt(argc, argv, "h?")) != -1) {
      switch (ch) {
      default:                  /* error */
        usage(argv[0]);
        exit(1);
        break;
    }
  }
  
  argc -= optind;
  argv += optind;
  
  if ((argc < 1) || !strcmp (argv[0], "-")) {
    sndFile = STDIN_FILENO;
    fprintf(stderr, "db_level monitor reading from stdin\n");
  } else {
    sndFile = open(argv[0], O_RDONLY, 0);
    fprintf(stderr, "db_level monitor reading from %s\n", argv[0]);
  }

  if (sndFile < 0) {
    perror ("open failed");
    exit(1);
  }

  signal(SIGALRM, pr);

  alarm(1);

  {
    int bufsize = REC_BUFSIZ;
    int readcount = bufsize;
    register double dBmax = 0, dB = 0;
    register u_int16_t max = 0, val = 0, i;
    buffer = malloc(bufsize);
    
    while (readcount) {
      readcount = bufsize;
      readcount = read(sndFile, buffer, readcount);
      if (readcount < 0) {
        perror ("read failed");
        break;
      }
/*       for (i = 0; i < readcount; i++) { */
/*         val = (u_int16_t) buffer[i]; */
/*         if (val > max) { */
/*           max = val; */
/*           dBmax = c_db(val); */
/*         } */
/*         dB = c_db (val); */
/*         fprintf(stderr, "\r%10.4f %10.4f", dB, dBmax); */

/*       } */
    }

  }

  return 0;
}
