/****************************-*-C-*-***********************************
 * $Id: display.h,v 1.5 2001/08/23 01:28:18 anarcat Exp anarcat $
 **********************************************************************
 * Display procedures declarations for rec
 **********************************************************************
 *   Copyright (C) 2001 The Anarcat <anarcat@anarcat.dyndns.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef DISPLAY_H
#define DISPLAY_H

/* set to sensible value in seconds to have eta display */
extern long int eta, real_toread;

__inline__ void update_display(long int readcount, /* bytes read */
                               float diff); /* time diff */

void display (long int now_read, /* what we have read now */
              long int toread,  /* how much data we have to read at all */
              int time,         /* time passed, in seconds */
              int eta);         /* time left, in seconds */

__inline__ void refresh_display ();        /* refresh display with current values */

#endif
