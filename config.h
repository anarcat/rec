/****************************-*-C-*-***********************************
 * $Id: config.h,v 1.3 2001/08/15 18:47:32 anarcat Exp anarcat $
 **********************************************************************
 * Configuration directives
 **********************************************************************
 *   Copyright (C) 2001 The Anarcat <anarcat@anarcat.dyndns.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

/* compile in AIFF support, no let makefile decide */
/* #define USE_AIFF */

/* enables compiling */
#undef NDEBUG

/* create a bufsize */
#ifndef REC_BUFSIZ
#define REC_BUFSIZ 2048
#endif

/* enable buffer size optimisation facilities */
#define OPTIMIZE_BUFSIZE 1

#endif
