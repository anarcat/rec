/****************************-*-C-*-***********************************
 * $Id: rec.c,v 0.59 2001/10/01 08:50:47 anarcat Exp $
 **********************************************************************
 * DSP recording utility for FreeBSD
 **********************************************************************
 *   Copyright (C) 2000 The Anarcat <anarcat@tao.ca>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

/*
 * This program can be used to record RAW (PCM) audio from the DSP device
 * 
 * "The default audio data format is 8 kHz/8-bit unsigned/mono
 * (/dev/dsp) or 8 kHz/mu-Law/mono (/dev/audio)."
 * 
 * "The /dev/dsp device uses 8-bit unsigned encoding while /dev/dspW
 * uses 16-bit signed little-endian (Intel) encoding and /dev/audio
 * uses logarithmic mu-law encoding."
 *
 * See the help (./rec -h) for more info
 *
 * Note that the resulting file can be easily converted to WAVE using sox:
 *
 * $ sox -c 2 -r 44100 -w -s file.raw file.wav
 */

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/soundcard.h>
#include <sys/ioctl.h>
#include <err.h>
#include <errno.h>
#include <signal.h>
#ifndef NDEBUG
#include <assert.h>
#include <sys/time.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef NO_DISPLAY
#include "display.h"
#endif
#include "convert.h"
#ifdef USE_AIFF
#include "aiff.h"
#endif

#define REC 0x1
#define PLAY 0x2
#define DFL_DEV "/dev/dsp"
#define DFL_FMT AFMT_S16_LE     /* default to 16 bits little endian */
#define DFL_CHAN 2
#define DFL_RATE 44100

/* these are global for signal handlers */
int snddev;                     /* sound device to read from */
int sndFile;                    /* file to write it to */

#ifdef USE_AIFF
AIFFSound aiffSnd;              /* sound to write */
#endif

#ifndef NDEBUG
struct timeval start;           /* start of recording */
struct timeval end;             /* end of recording */
#endif

/* handles an interrupt in raw readings */
void quit(int signum) {

#ifndef NDEBUG
  long int secs, msecs;
#endif
  fprintf(stderr, "\n");

#ifndef NDEBUG
  assert(!gettimeofday(&end, NULL));
  
  secs = end.tv_sec - start.tv_sec;
  msecs = end.tv_usec - start.tv_usec;
  if (msecs < 0) {              /* keep ms > 0 */
    secs--;
    msecs += 1000000;
  }
  assert (msecs >= 0);

  fprintf(stderr, "duration: %li.%6.6li secs\n", 
          secs,msecs);
#endif

  close(snddev);
  close(sndFile);
  if (signum > 0)
    fprintf(stderr, "finished with signal %d\n", signum);
  else
    fprintf(stderr, "finished\n");
  
  exit(0);
  
}

#ifdef USE_AIFF
/* handles interrupts from aiff routines */
void stopRec(int signum) {

  stopRecording(sndFile, &aiffSnd);
  
  fprintf(stderr, "\nwrote %d frames with %d bytes of raw sound data\n",
          aiffSnd.comm.numSampleFrames,
          aiffSnd.ssnd.ckDataSize);

  /* fd should be < 0 now, so this won't do anything */
  assert(aiffSnd.ssnd.fd <= 0);
  writeAIFFSound(sndFile, &aiffSnd, 0); /* bogus bufsize */
  quit(signum);
  
}
#endif

/**
 * Initialises the DSP device
 * 
 * Returns the fd of the open call
 **/
int initDSP(char* device, int *fmt, int *channels, 
            int *rate, int mode
#ifdef OPTIMIZE_BUFSIZE
, int *bufsize
#endif
            ) {

  int backup, fd = open(device, 
                        ((mode == REC) ? 
                         (O_RDONLY) : (O_WRONLY)));
  
  if (fd < 0) return fd;
  
  /* list allowed sample formats */
  if (ioctl(fd, SNDCTL_DSP_GETFMTS, &backup) == -1) {
    perror("can't read allowed sample formats");
    return 0;
  }

  if (!(backup & *fmt)) {   /* format not allowed */
    fprintf(stderr, "sample format not allowed by driver");
    errno = EINVAL;
    return 0;
  }
  
  /* set sample format */
  backup = *fmt;
  if (ioctl(fd, SNDCTL_DSP_SETFMT, fmt)) {
    perror ("setting sample format failed");
    return 0;
  }
  if (backup != *fmt)
    fprintf(stderr, "format changed from %d to %d", 
            backup, *fmt);
  
  /* set channel count */
  backup = *channels;
  if (ioctl(fd, SNDCTL_DSP_CHANNELS, channels) == -1) {
    /* Fatal error */
    perror("can't set channel count");
    return 0;
  }
  if (*channels != backup) {
    fprintf(stderr,
            "number of channels (%d) not supported, falling back to %d\n",
            backup, *channels);
  }

  /* set sample rate */
  backup = *rate;
  if (ioctl(fd, SNDCTL_DSP_SPEED, rate)==-1) {
    /* Fatal error */
    perror("can't set sample rate"); 
    return 0;
  }
  if (*rate != backup) {
    fprintf(stderr,
            "channel rate (%d) not supported, falling back to %d\n",
            backup, *rate);
  }

#ifdef OPTIMIZE_BUFSIZE
  if (ioctl(fd, SNDCTL_DSP_GETBLKSIZE, bufsize) == -1)
    fprintf(stderr, 
            "can't read optimal buffer size from driver\n");
#endif
  return fd;

} /* end initDSP() */

/* a basic usage */
void usage(char* argvo) {
  fprintf(stderr,
          "\
usage: %s [-r rate] [-c channels] [-s size | -b | -w] [-d dev] [-g size]
       [-h] [-m mode] "
#ifdef USE_AIFF
"[-p | -a] "
#endif
"[-t time | -f frames] file\n", argvo);
} /* end usage() */

/* more elaborate help */
void help(char* argvo) {
  usage(argvo);
  fprintf(stderr, "
where: (defaults)
-q: be quiet
-r rate: sets sample rate to rate (%d Hz)
-c channels: number of channels to use (%d)
-b: use unsigned 8 bits samples
-w: use signed 16 bits samples (default)
-s format: sample format (8 or 16 bits, -b or -w)
-d device: DSP device to use (%s)
-g size: buffer size to use ("
#ifdef OPTIMIZE_BUFSIZE
"detected"
#else
"%d bytes"
#endif
")
-f samples: number of samples to record (0, \"unlimited\")
-t time: maximum recording time (0, \"unlimited\")
-m {play|rec}: force mode (play or record, default by name)\n"
#ifdef USE_AIFF
          "-a: force AIFF output
-p: force RAW PCM output\n"
#endif
          "-h: gives this help

Records/plays a sound file"
#ifdef USE_AIFF
          " in the AIFF format if the filename ends with .aiff
or .aif or if the flag -a is specified, or "
#endif
          "in a RAW PCM format (signed "
#ifdef USE_AIFF
"\n"
#endif
"linear 2's complement, "
#ifndef USE_AIFF
"\n"
#endif
"little endian)"
#ifdef USE_AIFF
          " otherwise"
#endif
          ".

If time or a number of samples is specified, the recording will not be
longer than that (in seconds or frames), if not, rec will stop only for
a write failure or interruption (SIGINT/SIGHUP).

When conflicting flags (b, w, s, and f, time) are specified, the latter
overrides the former.

Note that the raw file can usually be converted using something like:
$ sox -c 2 -r 44100 -w -s file.raw file.wav

$Id: rec.c,v 0.59 2001/10/01 08:50:47 anarcat Exp $\n\n", 
          DFL_RATE, DFL_CHAN, DFL_DEV
#ifndef OPTIMIZE_BUFSIZE
          ,REC_BUFSIZ
#endif
          );
} /* end help */

/* main */
int main(int argc, char *argv[]) {

  short mode = REC;             /* mode... */
  int toread = -1;              /* what's left to read */
#ifdef USE_AIFF
  short aiffFormat = -1;        /* boolean */
#endif
  int bufsize = REC_BUFSIZ;

  int quiet = 0;
  
  long int real_toread = 0;
  
  char *device = DFL_DEV;
  int fmt = DFL_FMT,
    channels = DFL_CHAN,
    rate = DFL_RATE;
 
  int tmp;
#ifdef HAS_GETPROGRAMNAME
  char* progname = getprogramname();
#else
  char* progname = strdup(argv[0]);
#endif
  
  /*********************************************************************
   * Default recording mode
   ********************************************************************/
  if (!strcmp (argv[0]+strlen(argv[0])-3, "rec")) {
    mode = REC;
  } else if (!strcmp(argv[0]+strlen(argv[0])-4, "play")) {
    mode = PLAY;
  } else {
    fprintf(stderr,
            "called under weird name %s, assuming record functions\n", 
            argv[0]);
  }
  
  /*********************************************************************
   * Command line parsing - this is getting too big
   ********************************************************************/
  {                             /* parsing of the options */
    char ch;                    /* used by getopt */
    char *endptr = 0;
    
    while ((ch = getopt(argc, argv, "m:t:f:d:r:c:s:g:bwq"
#ifdef USE_AIFF
                        "ap"
#endif
                        "h?")) != -1) {
      switch (ch) {
      case 'q':
        quiet = 1;
        break;
      case 'm':
        if (!strcmp(optarg, "play")) {
          mode = PLAY;
        } else if (!strcmp(optarg, "rec")) {
          mode = REC;
        }
        break;
      case 'g':                 /* buffer size */
        tmp = strtol(optarg, &endptr, 0);
        if (*endptr) {          /* oups, did not stop at the end */
          usage(progname);
          fprintf (stderr, "invalid bufsize spec: %s\n", optarg);
          exit(1);
        } else if (!*optarg) {
          usage(progname);
          fprintf (stderr, "empty bufsize spec\n");
          exit(1);
        }
        
        if (tmp >= 0) {
          bufsize = tmp;
        } else {
          usage(progname);
          fprintf (stderr, 
                   "negative sample count specification: %d\n", 
                   tmp);
          exit(1);
        }

        break;
      case 'd':                 /* sound device */
        device = optarg;
        break;
      case 'r':                 /* sample rate */
        tmp = atoi(optarg);
        if (tmp > 0) rate = tmp;
        else {
          usage(progname);
          fprintf(stderr,
                  "invalid sample rate spec: %s\n", optarg);
          exit(1);
        }
        break;
      case 'c':                 /* channel count */
        tmp = atoi(optarg);
        if (tmp > 0) channels = tmp;
        else {
          usage(progname);
          fprintf(stderr,
                  "invalid channel count spec: %s\n", optarg);
          exit(1);
        }
          
        break;
      case 'f':                 /* frame count */
        tmp = strtol(optarg, &endptr, 0);
        if (*endptr) {          /* oups, did not stop at the end */
          usage(progname);
          fprintf (stderr, "invalid frame spec: %s\n", optarg);
          exit(1);
        } else if (!*optarg) {
          usage(progname);
          fprintf (stderr, "empty frame spec\n");
          exit(1);
        }
        
        if (tmp >= 0) 
          toread = BYTES_FROM_FRAMES
            (tmp, fmt, channels);
        else {
          usage(progname);
          fprintf (stderr, 
                   "negative sample count specification: %d\n", 
                   tmp);
          exit(1);
        }
        assert (toread >= 0);
        break;
      case 't':                 /* time count */
        toread = strtol(optarg, &endptr, 0);
        if (*endptr) {          /* oups, did not stop at the end */
          usage(progname);
          fprintf (stderr, "invalid time spec: %s\n", optarg);
          exit(1);
        } else if (!*optarg) {
          usage(progname);
          fprintf (stderr, "empty time spec\n");
          exit(1);
        }
        if (toread >= 0)
          toread = BYTES_FROM_TIME(toread, rate, 
                                   channels, fmt);
        else {
          usage(progname);
          fprintf(stderr, "negative time specification: %d\n", toread);
          exit(1);
        }
        assert (toread >= 0);
        break;
#ifdef USE_AIFF
      case 'a':                 /* force aiff output */
        aiffFormat = 1;
        break;
      case 'p':                 /* force pcm raw output */
        aiffFormat = 0;
        break;
#endif
      case 's':                 /* sample size (8 or 16) */
        tmp = atoi(optarg);
        if (tmp > 0) {
          if (tmp != 8 && tmp != 16) {
            usage(progname);
            fprintf(stderr,
                    "unsupported sampling format: %d, should be 8 or 16",
                    tmp);
            exit(1);
          }
          fmt = tmp;
        } else {
          usage(progname);
          fprintf(stderr,
                  "invalid sample format spec: %s\n", optarg);
          exit(1);
        }
        break;
      case 'b':                 /* byte samples (8 bits) */
        fmt = AFMT_U8;
        break;
      case 'w':                 /* long samples (16 bits) */
        fmt = AFMT_S16_LE;
        break;
      case 'h':                 /* long help */
        help(argv[0]);
        exit(1);
        break;
      case '?':                 /* usage */
      default:                  /* error */
        usage(argv[0]);
        exit(1);
        break;
      }
    }
  } /* end arg parsing */

  argc -= optind;
  argv += optind;

  /*********************************************************************
   * Open the ouput file
   ********************************************************************/
  if (argc < 1) {               /* examine filename */
    usage(progname);
    fprintf(stderr, "no filename given, aborting\n");
    exit(1);
  }
#ifdef USE_AIFF
  else if (aiffFormat < 0) {    /* we check this only if not set earlier */
    int len = strlen(argv[0]);
    if (len > 3) {
      if (!strncmp (&argv[0][len-4], ".aif", 4))
        aiffFormat = 1;
      else if (len > 4 && !strncmp (&argv[0][len-5], ".aiff", 5))
        aiffFormat = 1;
      else {
        if (strncmp (&argv[0][len-4], ".raw", 4))
          fprintf(stderr, "Unknown extension, using raw\n");
        aiffFormat = 0;
      }
    } else {
      fprintf(stderr, "No extension given, using raw\n");
      aiffFormat = 0;
    }
  }
#endif

  if (!strcmp (argv[0], "-")) { /* file is stdout */
#ifdef USE_AIFF
    if (aiffFormat)
      fprintf(stderr, 
              "can't seek stdout, aiff size spec might be invalid if interrupted\n");
#endif
    sndFile = STDOUT_FILENO;
  } else {
    sndFile = open(argv[0],
                   ((mode == REC) ? 
                    (O_WRONLY | O_CREAT | O_TRUNC) :
                    (O_RDONLY)),
                   ((mode == REC) ? 0644 : 0));
    if (sndFile < 0) {
      usage(progname);
      perror("can't open output file");
      exit(1);
    }
  }
  
  /* here we should have at least the filename */
  assert(argc >= 1);
  
  /*********************************************************************
   * Examine Time and set toread
   ********************************************************************/
  if (toread <= 0)              /* no time given */
#ifdef USE_AIFF
    /* here we should check for freespace in case of PCM records */
    if (aiffFormat) {
      toread = MAX_BYTES;
      if (!quiet)
        fprintf(stderr, "%s until INT (control-c) or %li seconds.\n",
                ( (mode == REC) ? "recording" : "playing" ),
                MAX_TIME(fmt, channels, rate));
    } else                      /* pcm recording */
#endif
      {
        toread = 0;             /* this should be get_freespace(path) */
        if (!quiet)
          fprintf(stderr, "%s until INT (control-c).\n", 
                  ( (mode == REC) ? "recording" : "playing" ));
      }
  else
    if (!quiet)
      fprintf(stderr,
              "%s %d seconds (%d frames), send INT (control-c) to stop\n",
              ( (mode == REC) ? "recording" : "playing" ),
              TIME_FROM_BYTES(toread, fmt,
                              channels, rate),
              FRAMES_FROM_BYTES(toread, fmt, channels));
  
  /*********************************************************************
   * Init DSP device and set format, get bufsize and check it
   ********************************************************************/
  tmp = bufsize;                /* backup */
  snddev = initDSP(device, &fmt, &channels, &rate, mode
#ifdef OPTIMIZE_BUFSIZE
, &bufsize
#endif
);
  if (snddev == 0) {
    usage(progname);
    perror("sound device configuration failed");
    exit(1);
  } else if (snddev < 0) {
    usage(progname);
    perror("cannot open sound device");
    exit(1);
  }

#ifdef OPTIMIZE_BUFSIZE
  if (tmp != REC_BUFSIZ) {
    if (tmp != bufsize) {
      fprintf(stderr, "driver says %d better than %d but we ignore it\n",
              bufsize, tmp);
      bufsize = tmp;
    }
  } else 
    if (tmp != bufsize) {
      fprintf(stderr, 
              "driver says %d better than %d for buffer size, believing it\n",
              bufsize, tmp);
    }
#endif
  if (!quiet)
    fprintf(stderr, "using a buffer size of %d bytes\n", bufsize);

#ifdef USE_AIFF
  aiffSnd.ssnd.fd = snddev;
#endif

  /* bufsize must be a multiple of (size / 8) * channels */
  {
    int mul = (fmt / 8) * channels;
    int rest = bufsize % mul;
    if (rest) {
      bufsize -= rest;
      fprintf(stderr,
              "rounding bufsize to a multiple of %d, (%d) to have complete samples\n", 
              mul, bufsize);
    }
  }

  /*********************************************************************
   * Print recording stats
   ********************************************************************/
  if (!quiet)
    fprintf(stderr, "\
Device: %s
Output format: %d Hz, %d channels, %d bits",
            device,
            rate,
            channels,
            fmt);

#ifdef USE_AIFF
  if (aiffFormat && !quiet) fprintf(stderr, " AIFF\n");
  else
#endif
    if (!quiet) {
      switch (fmt) {
      case AFMT_U8:
        fprintf(stderr, " raw unsigned\n");
        break;
      case AFMT_S16_LE:
        fprintf(stderr, " raw signed little-endian\n");
        break;
      case AFMT_S16_BE:
        fprintf(stderr, " raw signed big-endian\nTime and frame calculations may be invalid\n");
        break;
      default:
        fprintf(stderr, " unknown raw format: %d, good luck\n",
                fmt);
        break;
      }
    }
  
  if (toread <= 0) {
    toread = bufsize;
    real_toread = 0;
  } else {
    real_toread = toread;     /* set external real_toread for eta */
  }

#ifndef NO_DISPLAY
  if (!quiet) {
    /* compute inital eta */
    eta = TIME_FROM_BYTES(toread, fmt,
                          channels, rate);
    if (real_toread) fprintf(stderr, "   ");
    fprintf(stderr, "     bytes");
    if (real_toread) fprintf(stderr, "          ");
    fprintf(stderr, "    time\n      read");
    if (real_toread) fprintf(stderr, "/total     ");
    
    fprintf(stderr, "  passed");
    if (real_toread) fprintf(stderr, "/left");
    fprintf(stderr, "\n");
    refresh_display();
  }
#endif

  /*********************************************************************
   * Record the sample
   ********************************************************************/
#ifdef USE_AIFF  
  if (aiffFormat) {
    
    signal(SIGINT, stopRec);    /* handle signals smoothly */
    signal(SIGHUP, stopRec);

    aiffSnd.comm.numChannels = channels;
    aiffSnd.comm.sampleSize = fmt;
    aiffSnd.comm.sampleRate = rate;
    aiffSnd.ssnd.ckDataSize = toread + 8;
    updateSizeFields(&aiffSnd);

#ifndef NDEBUG
    assert(!gettimeofday(&start, NULL));
#endif
    if (writeAIFFSound(sndFile, &aiffSnd, bufsize) < 0)
      perror("error in writeAIFFSound");

    stopRec(0);
    
  } else
#endif
    {                           /* RAW recording */
      register long int readcount = 0;
      unsigned char *buffer = malloc(bufsize);

      if (!buffer) {
        perror ("can't allocate memory for read buffer");
        exit(1);
      }
    
      signal( SIGINT, quit );
      signal( SIGHUP, quit );
        
#ifndef NDEBUG
      assert(!gettimeofday(&start, NULL));
#endif
      
      while (!real_toread || toread > 0) {
        readcount = bufsize;
        if (real_toread && readcount > toread)
          readcount = toread;
        readcount = read(((mode == REC) ? (snddev) : (sndFile)),
                         buffer, readcount);
        if (readcount < 0) {
          perror("dspread");
          exit(1);
        } else if (readcount == 0) {
          break;
        }
        if (write(((mode == REC) ? (sndFile) : (snddev)),
                  buffer, readcount) < 0) {
          perror("failed to write to file");
          exit(1);
        }
        toread -= readcount;
#ifndef NO_DISPLAY
        if (!quiet)
          update_display(readcount, 
                         TIME_FROM_BYTES
                         ((float) readcount, fmt, channels, rate));
#endif
      }
      quit(0);
    }
  return 0;                     /* not reached */
} /* end main() */
