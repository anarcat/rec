#if 0
	file=`basename $0 .c`
	set -ex
	cc -g -O3 \
		-I/usr/local/include \
		-L/usr/local/lib \
		-L/usr/X11R6/lib \
		$file.c -o $file \
		-lX11 -lrfftw -lfftw -lm
	exit
#endif

#define PREFER_RECTS 0 /* XDrawSegments tiny bit faster than XFillRectangles */

/*
 *  Wed Feb 14 23:37:26 EET 2001 - junki
 *  Sat Feb 17 01:36:13 EET 2001 - junki - dabbledydab
 *  Sat Feb 17 19:15:55 EET 2001 - junki - label drag changes
 *  Sun Feb 18 21:16:50 EET 2001 - junki - audio config checks
 *  Mon Feb 19 09:13:53 EET 2001 - junki - history before fft
 *  Tue Feb 20 08:05:52 EET 2001 - junki - changes. some more.
 *  Fri Feb 23 20:36:14 EET 2001 - junki - segments 1 pixel too tall.
 *
 *  FFTW library from:
 *    ftp://ftp.fftw.org/pub/fftw/
 *    http://www.fftw.org/
 *
 * Hint:
 * $ sox foo.au -r 16000 -sw -t raw /dev/stdout | ./ascope3 -A /dev/stdin
 */

#define NUM_PEAKS 100

#define AUDDEV      "/dev/dsp"
#define SAMPLE_RATE 16000
#define VCR_LENGTH  (60 * 5) /* 5 minutes :: 9.6 MBytes @ 16000 Hz  */
#define REFRESH_HZ  10       /* initially selected */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <sys/soundcard.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <math.h>

#include <X11/Xos.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

static char *auddev = AUDDEV;
static int afd = -1;

#ifdef AFMT_S16_NE
#define OUR_AFMT AFMT_S16_NE
#else
/* i386 */
#define OUR_AFMT AFMT_S16_LE
#endif

typedef signed short sample;

static int
	mapped, visible, halted,

	button_width,
	label_height,
	graph_width,
	graph_height,

	notice_width,
	notice_height,

	bar_offset,
	bar_start,   /* first visible */
	bar_end,     /* last visible + 1 */
	bar_count,

	autorepeat_interval,
	playback_dir,
	playback_has_repeated,
	button_x,         /* last button (motion ?) x */

	peak_count = NUM_PEAKS,

	marker_hz = -1,
	marker = -1,

	vcr_loc, /* current position, record/playback */
	vcr_end, /* next write (record mode) */
	vcr_len, /* current size */

	vcr_max = SAMPLE_RATE * VCR_LENGTH,

	sample_rate = SAMPLE_RATE,
	refresh_hz = REFRESH_HZ,

	hz_step; /* between bars */

static FILE *VCR;
static void (*autorepeat)();

static char
	*notice_msg;

static struct timeval notice_time;

static
#if PREFER_RECTS
	XRectangle
#else
	XSegment
#endif
	*greens, *blacks, *blues;

static int *height;              /* for bars, not all always calculated */
static sample *sample_buf;
static int sample_cc, sample_cnt;

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

static char *fontnames[] = {
	NULL, "5x7", "5x8", "6x9", "6x10", "6x12", "6x13",
	"7x13", "7x14", "8x13", "8x16", "9x15", "fixed"
};

static Atom wmclose;
static Display *dpy;
static int screen, dpywidth, dpyheight, areawidth, areaheight;
static Window root, area, notice;
static XFontStruct *font;
static GC gc, gc_black, gc_green, gc_blue;
static XColor black, white, red, green, blue;

static char *window_name;

static enum { NONE, LEFT, RIGHT } channel;

#include <rfftw.h>

static rfftw_plan plan;

static fftw_real
	*fft_data,
	*spectrum,
	scale,
	fix_scale,
	fix_scale_was = 16000000.; /* XXX */

static int
	pushing_button(),
	push_button(XButtonEvent *),
	release_button(XButtonEvent *),
	do_read(),
	new_bar_start(int idx),
	open_audio();

static void
	init_calc(),
	hz_str(int, char **str, int *len, int pad),
	set_marker_hz(int hz),
	set_marker(int idx),
	do_labels(),
	title(),
	init_vcr(),
	vcr_restart(),
	do_vcr(int new_loc),
	set_notice(char *msg),
	set_error(),
	draw_notice(),
	remove_notice(),
	close_audio(),
	halt(),
	start(),
	restart(),
	set_bar_start(int idx),
	cb_chan(int arg, int active),
	cb_scale(int arg, int active),
	cb_rate(int arg, int active),
	cb_step(int arg, int active),
	cb_halt(int arg, int active),
	slide_visible(),
	repeat_step(),
	align_buttons(),
	paint_buttons(),
	init_buttons(),
	sync_buttons(),
	do_run();

static void
doh()
{
	*(int *) 0 = 0;
}

main(int argc, char **argv)
{
	extern int optind, opterr;
	extern char *optarg;
	int c, i, j;
	char *me;

	XSetWindowAttributes wattr;
	XTextProperty nameprop;
	XSizeHints normal;
	XWMHints wm;
	XClassHint class;
	XGCValues gcv;

	char *display = getenv("DISPLAY");
	char *geom = NULL;
	int geomspec = PSize;
	int areax = 0, areay = 0;

	opterr = 0;

	while ((c = getopt(argc, argv, "d:g:f:s:r:A:LRS:P:M:")) != -1) {
		switch (c) {
		default:
	baduse:
			fprintf(stderr,
"Usage: audio_spectrum [-d dpy][-g geom][-f font]\n"
"                      [-s sample_rate_Hz][-r refresh_Hz]\n"
"                      [-A audio_dev][-L(eft)][-R(ight)][-S scale]\n"
"                      [-P peak_count][-M mark_Hz]\n"
);
			exit(2);

		case 'd':
			display = optarg;
			break;
		case 'g':
			geom = optarg;
			break;
		case 'f':
			fontnames[0] = optarg;
			break;

		case 's':
			sample_rate = atoi(optarg);
			break;

		case 'r':
			refresh_hz = atoi(optarg);
			break;

		case 'A':
			auddev = optarg;
			break;

		case 'L':
			if (channel)
				goto baduse;
			channel = LEFT;
			break;

		case 'R':
			if (channel)
				goto baduse;
			channel = RIGHT;
			break;

		case 'S':
			fix_scale = atof(optarg);
			fix_scale_was = fix_scale;
			break;

		case 'P':
			peak_count = atoi(optarg);
			if (peak_count < 0)
				peak_count = 0;
			if (peak_count > NUM_PEAKS)
				peak_count = NUM_PEAKS;
			break;

		case 'M':
			set_marker_hz(atoi(optarg));
			break;

		}
	}
	if (optind != argc)
		goto baduse;

	if ((dpy = XOpenDisplay(display)) == NULL) {
		fprintf(stderr, "Cannot open display.\n");
		exit(1);
	}
	screen = DefaultScreen(dpy);
	root = RootWindow(dpy, screen);

	dpywidth  = DisplayWidth(dpy, screen);
	dpyheight = DisplayHeight(dpy, screen);

	for (i = 0; i < sizeof(fontnames) / sizeof(*fontnames); ++i) {
		if (fontnames[i]) {
			font = XLoadQueryFont(dpy, fontnames[i]);
			if (font)
				goto found;
		}
	}
	fprintf(stderr, "Cannot load any suitable font.\n");
	exit(1);
found:

	init_vcr();
	init_calc();
	init_buttons();

	label_height = 1 + font->ascent + font->descent + 1;

	graph_width = 5000 / hz_step;
	graph_height = 150;

	areaheight = graph_height + label_height;
	areawidth  = graph_width + button_width;

	if (areawidth > dpywidth)
		areawidth = dpywidth;

	if (areaheight > dpyheight)
		areaheight = dpyheight;

	if (geom) {
		i = XParseGeometry(geom, &areax, &areay, &areawidth, &areaheight);

		if (i == NoValue) {
			fprintf(stderr, "Bad geometry.\n");
			exit(2);
		}
		if (i & XNegative)
			areax += dpywidth - areawidth;
		if (i & YNegative)
			areay += dpyheight - areaheight;
		if (i & (XValue | YValue))
			geomspec |= USPosition;
		if (i & (WidthValue | HeightValue))
			geomspec |= USSize;
	}

	graph_width  = areawidth  - button_width;
	graph_height = areaheight - label_height;

	black.pixel = BlackPixel(dpy, screen);
	white.pixel = WhitePixel(dpy, screen);

	green.green = ~0;
	blue.blue = ~0;
	red.red = ~0;

	if (!XAllocColor(dpy, DefaultColormap(dpy, screen), &red)
	 || !XAllocColor(dpy, DefaultColormap(dpy, screen), &green)
	 || !XAllocColor(dpy, DefaultColormap(dpy, screen), &blue)) {
		fprintf(stderr, "Color problems.\n");
		exit(2);
	}

	gcv.function = GXcopy;
	gcv.font = font->fid;
	gcv.plane_mask = AllPlanes;
	gcv.graphics_exposures = False;
	gcv.line_width = 0;

	gcv.background = black.pixel;
	gcv.foreground = white.pixel;
	gc = XCreateGC(dpy, root,
		GCFunction | GCFont | GCPlaneMask | GCGraphicsExposures |
		GCLineWidth | GCBackground | GCForeground, &gcv);

	gcv.background = white.pixel;
	gcv.foreground = black.pixel;
	gc_black = XCreateGC(dpy, root,
		GCFunction | GCFont | GCPlaneMask | GCGraphicsExposures |
		GCLineWidth | GCBackground | GCForeground, &gcv);

	gcv.background = black.pixel;
	gcv.foreground = green.pixel;
	gc_green = XCreateGC(dpy, root,
		GCFunction | GCFont | GCPlaneMask | GCGraphicsExposures |
		GCLineWidth | GCBackground | GCForeground, &gcv);

	gcv.line_style = LineDoubleDash;
	gcv.background = white.pixel;
	gcv.foreground = blue.pixel;
	gc_blue = XCreateGC(dpy, root,
		GCLineStyle |
		GCFunction | GCFont | GCPlaneMask | GCGraphicsExposures |
		GCLineWidth | GCBackground | GCForeground, &gcv);

	wattr.border_pixel = black.pixel;
	wattr.background_pixel = black.pixel;
	wattr.bit_gravity = ForgetGravity;
	wattr.backing_store = NotUseful;
	wattr.event_mask = StructureNotifyMask | VisibilityChangeMask |
		ButtonPressMask | ButtonReleaseMask | ButtonMotionMask |
		ExposureMask | KeyPressMask;

	area = XCreateWindow(dpy, root,
			areax, areay, areawidth, areaheight, 1,
			CopyFromParent, InputOutput, CopyFromParent,
			CWBorderPixel | CWBackPixel | CWBitGravity |
			CWBackingStore | CWEventMask,
			&wattr);

	wattr.border_pixel = blue.pixel;
	wattr.background_pixel = white.pixel;
	wattr.win_gravity = CenterGravity;
	wattr.save_under = True;
	wattr.event_mask = ExposureMask;

	notice_width  = font->max_bounds.width * 25;
	notice_height = (font->ascent + font->descent) * 3;

	notice = XCreateWindow(dpy, area,
			graph_width  / 2 - notice_width  / 2,
			graph_height / 2 - notice_height / 2,
			notice_width, notice_height, 0,
			CopyFromParent, InputOutput, CopyFromParent,
			CWBorderPixel | CWBackPixel | CWWinGravity |
			CWSaveUnder | CWEventMask,
			&wattr);

	normal.flags = geomspec;

	normal.x      = areax;
	normal.y      = areay;
	normal.width  = areawidth;
	normal.height = areaheight;
#if 0
	normal.flags = geomspec | PResizeInc | PBaseSize;
	normal.width_inc   = meter_size;
	normal.height_inc  = meter_size;
	normal.base_width  = 0;
	normal.base_height = 0;
#endif
	normal.flags |= PMinSize;
	normal.min_width   = 8 + button_width;
	normal.min_height  = label_height + 8;

	wm.flags = InputHint | StateHint | WindowGroupHint | IconWindowHint;
	wm.input = True;
	wm.initial_state = NormalState;
	wm.window_group = area;
	wm.icon_window = None;

	me = "audio_spectrum";
	class.res_name = "audio_spectrum";
	class.res_class = "Analyzer";

	XStringListToTextProperty(&me, 1, &nameprop);

	XSetWMProperties(dpy, area,
		&nameprop, &nameprop, argv, argc, &normal, &wm, &class);

	wmclose = XInternAtom(dpy, "WM_DELETE_WINDOW", False);

	XSetWMProtocols(dpy, area, &wmclose, 1);

	XMapWindow(dpy, area);

	XFetchName(dpy, area, &window_name);

	do_run();
}

static void
init_calc()
{
	sample_cnt = sample_rate / refresh_hz;

	hz_step = sample_rate / sample_cnt;

	if (plan)
		rfftw_destroy_plan(plan);

	plan = rfftw_create_plan(sample_cnt, FFTW_REAL_TO_COMPLEX,
#if 0
		FFTW_MEASURE
#else
		FFTW_ESTIMATE
#endif
	);

#define FOO(v,c) do{ if (v) free(v); v = calloc(sizeof(*(v)), (c)); } while(0)

	FOO(sample_buf, 2 * sample_cnt); /* 2 because of stereo read() */

	FOO(fft_data, sample_cnt);
	FOO(spectrum, sample_cnt); /* also fft_out temporarily */

	bar_count = sample_cnt / 2;
	bar_start = 0;
	bar_end   = bar_count;

	FOO(height, bar_count + 1);
	FOO(greens, bar_count + 1);
	FOO(blacks, bar_count + 1);
	FOO(blues,  bar_count + 1);

#undef FOO
}

static int
do_calc()
{
	sample *pcm = sample_buf;
	int i, n = sample_cnt;
	fftw_real *in  = fft_data;
	fftw_real *out = spectrum; /* reused as temporary storage */

	for (i = 0; i < n; ++i)
		in[i] = pcm[i];

	rfftw_one(plan, in, out);

#if 0
#define FOO(x) (out[x] * out[x])
#else
#define FOO(x) fabs(out[x])
#endif

	out[0] = FOO(0);

	for (i = 1; i < n / 2; ++i)
		out[i] = FOO(i) + FOO(n - i);

	out[i] = FOO(i);

#undef FOO
}

static void
hz_str(int hz, char **s, int *n, int pad)
{
	static char buf[sizeof(" 4294967295 ")];
	char *cp = &buf[sizeof(buf)];

	*--cp = 0;
	if (pad)
		*--cp = pad;
	do {
		*--cp = '0' + hz % 10;
	} while (hz /= 10);
	if (pad)
		*--cp = pad;

	*s = cp;
	*n = &buf[sizeof(buf)] - cp - 1;
}

static void
do_graph()
{
	int i, x, h;
	int peak = 0, num_peaks = 0;
#if PREFER_RECTS
	XRectangle
#else
	XSegment
#endif
		*brect, *grect, *mrect;
	int graph_active_width;
	struct {
		int x, h;
		int hz, tw;
	} peaks[NUM_PEAKS + 1]; /* 1 for possible spillover */

	if (graph_height <= 0)
		return;

	graph_active_width = min(graph_width, bar_end - bar_start);

	scale = fix_scale / hz_step;
	if (scale <= 0) {
		scale = 1;
		for (i = bar_start; i < bar_end; ++i) {
			if (scale < spectrum[i])
				scale = spectrum[i];
		}
	}
	for (i = bar_start; i < bar_end; ++i) {
		height[i] = graph_height * spectrum[i] / scale;
	}

	brect = blacks;
	grect = greens;
	mrect = blues;

	for (x = 0, i = bar_start; i < bar_end; ++i, ++x) {

		h = height[i];
		if (h < 0)
			h = 0;
		if (h > graph_height)
			h = graph_height;

		if (h < graph_height) {
			if (i == marker) {
#if PREFER_RECTS
				mrect->x = x;
				mrect->y = 0;
				mrect->width = 1;
				mrect->height = graph_height - h;
#else
				mrect->x1 = x;
				mrect->y1 = 0;
				mrect->x2 = x;
				mrect->y2 = graph_height - h - 1;
#endif
				mrect++;
			} else {
#if PREFER_RECTS
				brect->x = x;
				brect->y = 0;
				brect->width = 1;
				brect->height = graph_height - h;
#else
				brect->x1 = x;
				brect->y1 = 0;
				brect->x2 = x;
				brect->y2 = graph_height - h - 1;
#endif
				brect++;
			}
		}
		if (h > 0) {
			if (i == marker) {
#if PREFER_RECTS
				mrect->x = x;
				mrect->y = graph_height - h;
				mrect->width = 1;
				mrect->height = h;
#else
				mrect->x1 = x;
				mrect->y1 = graph_height - h;
				mrect->x2 = x;
				mrect->y2 = graph_height - 1;
#endif
				mrect++;
			} else {
#if PREFER_RECTS
				grect->x = x;
				grect->y = graph_height - h;
				grect->width = 1;
				grect->height = h;
#else
				grect->x1 = x;
				grect->y1 = graph_height - h;
				grect->x2 = x;
				grect->y2 = graph_height - 1;
#endif
				grect++;
			}
		}
		if (i == 0)
			continue;

		/* save peak_count highest peaks, spill lesser peaks */

		if (height[i] > height[i - 1] || (height[i] == peak && peak)) {
			peak = h;
			continue;
		}
		if (height[i] < height[i - 1] && peak) {

			/* linked list better ? */

			int j, k, xx, hz, tw;

			for (j = 0; j < num_peaks && peaks[j].h >= peak; ++j)
				;
			for (k = num_peaks - 1; k >= j; --k)
				peaks[k + 1] = peaks[k];

			hz = hz_step * (i - 1);

			tw = font->max_bounds.width
			   * (hz<10 ? 1 : hz<100 ? 2 : hz<1000 ? 3 : hz<10000 ? 4 : 5);

			xx = x;
			if (xx > graph_active_width - tw)
				xx = graph_active_width - tw;

			peaks[j].x = xx;
			peaks[j].h = peak;
			peaks[j].hz = hz;
			peaks[j].tw = tw;

			if (num_peaks < peak_count)
				num_peaks++;

			peak = 0;
		}
	}

	if (brect != blacks)
#if PREFER_RECTS
		XFillRectangles
#else
		XDrawSegments
#endif
			(dpy, area, gc_black, blacks, brect - blacks);

	if (grect != greens)
#if PREFER_RECTS
		XFillRectangles
#else
		XDrawSegments
#endif
			(dpy, area, gc_green, greens, grect - greens);

	if (mrect != blues)
#if PREFER_RECTS
		XFillRectangles
#else
		XDrawSegments
#endif
			(dpy, area, gc_blue, blues, mrect - blues);

	for (i = 0; i < num_peaks; ++i) {
		if (peaks[i].h) {
			int j, h = peaks[i].h;
			int len;
			char *s;

			if (h > graph_height - font->ascent)
				h = graph_height - font->ascent;

			hz_str(peaks[i].hz, &s, &len, 0);

			XDrawString(dpy, area, gc,
				peaks[i].x, graph_height - h, s, len);

			/* discard overlapping peak labels */

			h -= font->ascent + font->descent - 1;

			for (j = i + 1; j < num_peaks; ++j) {

				if (peaks[j].h <= h)
					continue;

				if (peaks[j].x + peaks[j].tw <= peaks[i].x
				 || peaks[j].x >= peaks[i].x + peaks[i].tw)
					continue;

				peaks[j].h = 0;
			}
		}
	}
}

static void
do_labels()
{
	int endx = graph_width;
	int i, x, lastx, hz, tw;
	int len;
	char *s;

	XClearArea(dpy, area, 0, graph_height + 1,
		graph_width, font->ascent + font->descent + 1, False);

	lastx = 0;

	if (bar_start != 0) {

		tw = 4 * font->max_bounds.width;
		XDrawString(dpy, area, gc,
			lastx, graph_height + 1 + font->ascent + 1, "... ", 4);

		lastx += tw;
	}
	if (bar_end != bar_count) {

		tw = 4 * font->max_bounds.width;
		XDrawString(dpy, area, gc,
			endx - tw, graph_height + 1 + font->ascent + 1, " ...", 4);

		endx -= tw;
	}
	for (i = bar_start; i < bar_end; ++i) {
		if (lastx >= endx)
			break;

		hz = hz_step * i;

		if (hz % 100)
			continue;

		hz_str(hz, &s, &len, 0);

		tw = len * font->max_bounds.width;
		x = (i - bar_start) - tw / 2;
		if (x + tw > endx)
			break;
		if (x < lastx)
			continue;

		XDrawString(dpy, area, gc,
			x, graph_height + 1 + font->ascent + 1, s, len);

		lastx = x + tw + font->max_bounds.width / 2;
	}
	if (marker >= bar_start && marker < bar_end) {

		hz_str(marker_hz, &s, &len, ' ');

		tw = len * font->max_bounds.width;
		x = (marker - bar_start) - tw / 2;
		if (x < 0)
			x = 0;
		if (x > graph_width - tw)
			x = graph_width - tw;

		XDrawImageString(dpy, area, gc_black,
			x, graph_height + 1 + font->ascent + 1, s, len);
	}
}

static void
title()
{
	char *p = malloc(strlen(window_name) + 128);

	strcpy(p, window_name);

	if (vcr_loc == vcr_end) {
		if (channel == RIGHT)
			strcat(p, ": right");
		else
		if (channel == LEFT)
			strcat(p, ": left");
	} else {
		int off = vcr_end - vcr_loc;

		if (off < 0)
			off += vcr_len;

		sprintf(p + strlen(p), ": T-%d.%02ds",
			off / sample_rate, (100 * off / sample_rate) % 100);
	}
	XStoreName(dpy, area, p);
	free(p);
}

static int
config_audio_dev()
{
	int n, nchan;
	int fd = afd;

#if 0
	/* DSP_NONBLOCK */
#endif

#if 1
	if (ioctl(fd, SNDCTL_DSP_RESET, NULL) == -1)
		return (False);
#endif

	n = OUR_AFMT;
	if (ioctl(fd, SNDCTL_DSP_SETFMT, &n) == -1)
		return (False);

	errno = 0;
	if (n != OUR_AFMT)
		return (False);

	nchan = (channel == RIGHT) ? 2 : 1;
	n = nchan;
	if (ioctl(fd, SNDCTL_DSP_CHANNELS, &n) == -1)
		return (False);

	errno = 0;
	if (n != nchan)
		return (False);

	n = sample_rate;
	if (ioctl(fd, SNDCTL_DSP_SPEED, &n) == -1)
		return (False);

	n -= sample_rate;
	if (n)
		printf("sample_rate error %d Hz of %d Hz\n", n, sample_rate);

	/* _last_ one, not critical, just try getting each chunk timely */

#if 1
	n = 13;
	while ((1 << n) > sample_cnt * sizeof(sample) / 2 && n > 8)
		n--;
	n |= (~0 << 16);

	if (ioctl(fd, SNDCTL_DSP_SETFRAGMENT, &n) == -1)
		return (False);
#endif

	return (True);
}

static int
open_audio()
{
	int flags;
	struct stat st;

	if (afd >= 0)
		doh();

	afd = open(auddev, O_RDWR, 0);
	if (afd == -1)
		return (False);

	/*
	 * if it is a regular file or fifo etc, or does not understand
	 * audio ioctls, assume sample_rate etc correct.
	 * chr: native audio or pty; socket: EsounD and such.
	 */
	if (fstat(afd, &st) == 0 && (S_ISCHR(st.st_mode) || S_ISSOCK(st.st_mode))) {

		if (!config_audio_dev() && errno != ENOTTY)
			return (False);
	}
#if 1
	errno = 0;
	flags = fcntl(afd, F_GETFL, 0);
	fcntl(afd, F_SETFL, flags | O_NONBLOCK);
	if (errno)
		return (False);
#endif

	read(afd, sample_buf, 4); /* select() might not start record */
	sample_cc = 0;

	if (channel == NONE)
		channel = LEFT;

	return (True);
}

static int
do_read()
{
	int cc, n;

	cc = sample_cnt * sizeof(sample);
	if (channel == RIGHT)
		cc *= 2;

	if (sample_cc >= cc)
		doh();

	n = read(afd, (char *) sample_buf + sample_cc, cc - sample_cc);

	/* error ? */

	if (n == -1) {
		set_error();
		halt();
		return (False);
	}
	/* EOF ? (from regular file etc) */

	if (n == 0) {
		halt();
		return (False);
	}
	sample_cc += n;

	if (sample_cc < cc)
		return (False);

	if (channel == RIGHT)
		for (n = 0; n < sample_cnt; ++n)
			sample_buf[n] = sample_buf[2*n+1];

	sample_cc = 0;

	return (True);
}

static void
close_audio()
{
	if (afd != -1) {
		close(afd);
		afd = -1;
	}
	sample_cc = 0;
}

static void
init_vcr()
{
	if (VCR)
		fclose(VCR);

	VCR = tmpfile();
	vcr_loc = vcr_end = vcr_len = 0;
}

static void
vcr_restart()
{
	if (!VCR)
		return;

	fseek(VCR, sizeof(*sample_buf) * vcr_end, SEEK_SET);
	vcr_loc = vcr_end;
}

static void
vcr_save()
{
	int n;

	if (!VCR)
		return;

	n = fwrite(sample_buf, sizeof(*sample_buf), sample_cnt, VCR);
	if (n > 0)
		vcr_loc += n;
	else
		set_error();

	if (vcr_len < vcr_loc)
		vcr_len = vcr_loc;

	if (vcr_loc >= vcr_max) {
		vcr_loc = 0;
		fseek(VCR, 0, SEEK_SET);
	}
	vcr_end = vcr_loc;
}

static void
load_vcr(int new_loc)
{
	int n, m;

	if (!VCR)
		return;

	if (new_loc < 0)
		new_loc = vcr_len + new_loc;
	if (new_loc >= vcr_len)
		new_loc = new_loc - vcr_len;

	vcr_loc = new_loc;
	fseek(VCR, sizeof(*sample_buf) * vcr_loc, SEEK_SET);

	n = fread(sample_buf, sizeof(*sample_buf), sample_cnt, VCR);
	if (n < 0)
		doh();

	vcr_loc += n;

	if (n < sample_cnt) {
		fseek(VCR, 0, SEEK_SET);

		m = fread(sample_buf + n, sizeof(*sample_buf), sample_cnt - n, VCR);
		if (m < 0)
			doh();

		vcr_loc = m;
		n += m;
	}
	if (vcr_loc >= vcr_max)
		vcr_loc = 0;

	fseek(VCR, sizeof(*sample_buf) * vcr_loc, SEEK_SET);

	/* XXX write samples back to soundcard */

	if (n < sample_cnt)
		bzero(sample_buf + n, sizeof(*sample_buf) * (sample_cnt - n));

	do_calc();
	do_graph();
}

static void
do_vcr(int new_loc)
{
	int loc = vcr_loc;
	int wrap = 0;

	if (vcr_loc == vcr_end && new_loc == loc)
		wrap = 1;

	halt();
	load_vcr(new_loc);
	title();

	if (vcr_loc == vcr_end && new_loc == loc - 2 * sample_cnt)
		wrap = 1;

	if (wrap)
		set_notice("Data wraps over");
}

static void
set_notice(char *msg)
{
	notice_msg = msg;
	XMapWindow(dpy, notice);
	XClearWindow(dpy, notice);
	draw_notice();
	XFlush(dpy);

	gettimeofday(&notice_time, NULL);
	notice_time.tv_sec++;
}

static void
set_error()
{
	switch (errno) {
	case ENOSPC:
		set_notice("DISK FULL");
		break;
	case 0:
		set_notice("Unsupported device");
		break;
	default:
		set_notice(strerror(errno));
		break;
	}
	notice_time.tv_sec += 5;
}

static void
draw_notice()
{
	char *msg;

	if (msg = notice_msg) {
		int th = font->ascent + font->descent;
		int tw = strlen(msg) * font->max_bounds.width;

		XDrawString(dpy, notice, gc_black,
			notice_width  / 2 - tw / 2,
			notice_height / 2 - th / 2 + font->ascent + 1,
			msg, strlen(msg));
	}
}

static void
remove_notice()
{
	XUnmapWindow(dpy, notice);
	notice_msg = NULL;
}

static void
halt()
{
	halted = True;
	close_audio();
	sync_buttons();
}

static void
start()
{
	if (afd >= 0 || open_audio()) {
		halted = False;

		vcr_restart();
		sync_buttons();
		title();
	} else {
		set_error();
		halt();
	}
}

static void
restart()
{
	close_audio();
	start();
}

static int
new_bar_start(int i)
{
	int bar_doh_this_gets_tiresome = bar_start;

	if (graph_width >= bar_count) {
		bar_start = 0;
		bar_end = bar_count;
	} else {
		bar_start = i;

		if (bar_start < 0)
			bar_start = 0;
		if (bar_start > bar_count - graph_width)
			bar_start = bar_count - graph_width;

		bar_end = bar_start + graph_width;
		if (bar_end > bar_count)
			bar_end = bar_count;
	}
	return (bar_start != bar_doh_this_gets_tiresome);
}

static void
set_bar_start(int i)
{
	if (new_bar_start(i)) {
		do_graph();
		do_labels();
		sync_buttons();
	}
}

static void
set_marker_hz(int hz)
{
	marker_hz = hz;
}

static void
set_marker(int idx)
{
	if (idx < 0)
		idx = 0;
	if (idx > bar_count)
		idx = bar_count;

	marker = idx;
	marker_hz = marker * hz_step;
}

static void
do_run()
{
	int xfd = ConnectionNumber(dpy);
	XEvent e;
	int n, drag_x, drag_start;
	int dragging = False;
	int need_clear = False;

	if (marker_hz >= 0)
		marker = marker_hz / hz_step;

	sync_buttons();

	for (;;) {
		if (mapped && !XPending(dpy)) {

			/* half-hearted avoidance of X write backlog. SND LOWAT/BUF XXX */

			struct timeval tv, *tvp;
			fd_set rset, wset, *wsetp;
			int graph_pending = False;

			if (!halted && afd < 0)
				start();

			FD_ZERO(&rset);
			FD_ZERO(&wset);

			wsetp = (afd >= 0) ? &wset : NULL;

			for ( ; !QLength(dpy); XFlush(dpy)) {

				/* afd sometimes has changed here */

				if (afd >= 0)
					FD_SET(afd, &rset);
				if (wsetp)
					FD_SET(xfd, wsetp);
				FD_SET(xfd, &rset);

				tvp = NULL;

				if (autorepeat) {
					tv.tv_sec = 0;
					tv.tv_usec = autorepeat_interval;
					tvp = &tv;
				} else if (notice_msg) {
					tv.tv_sec = 1;
					tv.tv_usec = 0;
					tvp = &tv;
				} else if (halted)
					break;

				n = select(max(afd, xfd) + 1, &rset, wsetp, NULL, tvp);
				if (n < 0)
					doh();

				if (notice_msg) {
					gettimeofday(&tv, NULL);
					if (tv.tv_sec >  notice_time.tv_sec
					 || tv.tv_sec == notice_time.tv_sec
					 && tv.tv_usec > notice_time.tv_usec)
						remove_notice();
				}
				if (n == 0) {
					if (autorepeat)
						(*autorepeat)();
					continue;
				}
				if (FD_ISSET(xfd, &rset))
					break;

				if (wsetp && FD_ISSET(xfd, wsetp))
					wsetp = NULL;

				if (afd >= 0 && FD_ISSET(afd, &rset)) {

					FD_CLR(afd, &rset); /* anticipate close_audio() */

					if (do_read()) {
						vcr_save();
						do_calc(); /* XXX optim */

						if (wsetp == NULL) {
							if (visible) {
								wsetp = &wset;
								do_graph();
							}
							graph_pending = False;
						} else {
							graph_pending = True; /* XXX reduce peaks ? */
						}
					}
				}
			}
			if (graph_pending)
				do_graph();
		}
		XNextEvent(dpy, &e);

		switch (e.type) {

		default:
			fprintf(stderr, "event %d ?\n", e.type);
			break;

		case ClientMessage:
			exit(0);
			break;

		case ReparentNotify:
		case CreateNotify:
		case DestroyNotify:
			break;

		case MapNotify:
		case UnmapNotify:
			if (e.xmap.window == area)
				mapped = e.type == MapNotify;
			break;

		case VisibilityNotify:
			if (e.xvisibility.window == area)
				visible = e.xvisibility.state != VisibilityFullyObscured;
			break;

		case ConfigureNotify:
			if (e.xconfigure.window == area) {
				areawidth  = e.xconfigure.width;
				areaheight = e.xconfigure.height;

				graph_width  = areawidth  - button_width;
				graph_height = areaheight - label_height;

				new_bar_start(bar_start);
				align_buttons();
				need_clear = True; /* grumble */

#if 0 /* ForgetGravity: one Expose coming up... */
				paint_buttons();
				do_graph();
				do_labels();
#endif
			}
			break;

		case Expose:
			if (e.xexpose.count == 0) {
				int n = False;
				int a = False;

				do {
					if (e.xexpose.window == area)
						a = True;
					if (e.xexpose.window == notice)
						n = True;
				} while (XCheckTypedEvent(dpy, Expose, &e));

				if (a) {
					if (need_clear) {
						need_clear = False;
						XClearWindow(dpy, area);
					}
					paint_buttons();
					do_graph();
					do_labels();
				}
				if (n)
					draw_notice();
			}
			break;

		case ButtonPress:
			button_x = e.xbutton.x;

			if (push_button(&e.xbutton))
				break;

			if (e.xbutton.button == 1 && e.xbutton.y >= graph_height) {

				drag_start = bar_start; /* label pressed */
				drag_x = button_x;
				dragging = 1;

			} else if (e.xbutton.button == 1) {

				set_marker(bar_start + button_x);
				halt();
				do_graph();
				do_labels();
			} else {
				if (halted)
					start();
				else
					halt();
			}
			break;

		case MotionNotify:
			n = False;
			do {
				if (e.xmotion.state & Button1Mask) {
					button_x = e.xmotion.x;
					n = True;
				}
			} while (XCheckTypedEvent(dpy, MotionNotify, &e)); /* purge */

			if (!n)
				break;

			if (pushing_button())
				break;

			if (dragging) {
				dragging = 2; /* press at drag_x, motion into button_x */

				set_bar_start(drag_start + drag_x - button_x);
				autorepeat_interval = 10000;
				autorepeat = slide_visible;
			} else {
				set_marker(bar_start + button_x);
				halt();
				do_graph();
				do_labels();
				autorepeat_interval = 10000;
				autorepeat = slide_visible;
			}
			break;

		case ButtonRelease:
			button_x = e.xbutton.x;

			if (release_button(&e.xbutton))
				break;

			if (e.xbutton.button == 1 && dragging == 1) {

				/* center graph if label clicked, not dragged */

				set_bar_start(bar_start + button_x - graph_width / 2);
			}
			autorepeat = NULL;
			dragging = False;
			sync_buttons();
			break;

		case KeyPress:
			n = XLookupKeysym(&e.xkey, 0);
			switch (n) {

			case XK_q:
				exit(0);

			case XK_space:
				if (halted)
					restart();
				else
					halt();
				break;

			case XK_l:
			case XK_r:
				channel = (n == XK_r) ? RIGHT : LEFT;
				if (!halted)
					restart();
				break;

			case XK_Home:
				do_vcr(0);
				break;

			case XK_BackSpace:
			case XK_Delete:
				do_vcr(vcr_loc - 2 * sample_cnt);
				break;

			case XK_Return:
				do_vcr(vcr_loc);
				break;

			case XK_End:
				do_vcr(vcr_end - sample_cnt);
				break;

			case XK_Down:
				set_bar_start(0);
				break;

			case XK_Left:
				set_bar_start(bar_start - graph_width / 5);
				break;

			case XK_Right:
				set_bar_start(bar_start + graph_width / 5);
				break;

			case XK_Up:
				set_bar_start(bar_count - graph_width);
				break;
			}
			break;

		}
	}
	doh();
}

static void
slide_visible()
{
	if (button_x < 0)
		set_bar_start(bar_start - 1);
	else
	if (button_x >= graph_width)
		set_bar_start(bar_start + 1);
}

static void
cb_chan(int arg, int active)
{
	if (!active)
		return;

	if (channel == RIGHT)
		channel = LEFT;
	else
		channel = RIGHT;

	close_audio();

	set_notice((channel == RIGHT) ? "RIGHT CHANNEL" : "LEFT CHANNEL");
	if (!halted)
		restart();
}

static void
cb_scale(int arg, int active)
{
	if (!active)
		return;

	if (fix_scale) {
		fix_scale_was = fix_scale;
		fix_scale = 0;
	} else {
		fix_scale = fix_scale_was;
	}
	set_notice(fix_scale ? "ABSOLUTE SCALE" : "RELATIVE SCALE");
	do_graph();
}

static void
cb_rate(int arg, int active)
{
	int newhz;
	int cent;
	static char buf[128];

	if (!active)
		return;

	switch (refresh_hz) {
	case 1:
		newhz = arg > 0 ? 2 : 1;
		break;
	case 2:
		newhz = arg > 0 ? 5 : 1;
		break;
	case 5:
		newhz = arg > 0 ? 10 : 2;
		break;
	case 10:
		newhz = arg > 0 ? 20 : 5;
		break;
	default:
		newhz = arg > 0 ? 20 : 10;
		break;
	}
	sprintf(buf, "PERIOD %d Hz", newhz);
	set_notice(buf);

	if (refresh_hz != newhz) {
		refresh_hz = newhz;

		cent = (bar_start + (bar_end - bar_start) / 2) * hz_step;

		/* if there is a marker, center into it */

		if (marker_hz >= 0)
			cent = marker_hz;

		close_audio();
		init_calc();

		if (marker_hz >= 0)
			marker = marker_hz / hz_step;

		new_bar_start(cent / hz_step - graph_width / 2);

		XClearArea(dpy, area, 0, 0, graph_width, areaheight, False);
		do_labels();

		if (halted)
			load_vcr(vcr_loc - sample_cnt);
		else
			restart();
	}
}

static void
repeat_step()
{
	playback_has_repeated = True;

	do_vcr(vcr_loc + (playback_dir - 1) * sample_cnt);
	autorepeat_interval = 1000;
}

static void
cb_step(int arg, int active)
{
	if (active) {
		halt();
		playback_has_repeated = False;
		playback_dir = arg;
		autorepeat_interval = 999999;
		autorepeat = repeat_step;
	} else {
		autorepeat = NULL;
		if (!playback_has_repeated)
			do_vcr(vcr_loc + (arg - 1) * sample_cnt);
	}
}

static void
cb_halt(int arg, int active)
{
	if (active)
		halt();
	else
		restart();
}

static unsigned long shades[4];
static GC btn_gc;

static XFontStruct *btn_font;

struct button {
	struct button *next;
	int x, y, w, h;
	char *text;
	int bw, tx, ty, tw;
	enum { INSTANT, TOGGLE } type;
	int sunken;
	int active;
	void (*callback)(int, int);
	int arg;
};

static struct button *buttons, *button_halt;

static struct button *
new_button(char *text, int type, void (*f)(), int arg)
{
	struct button **pp, *btn = calloc(1, sizeof(*btn));

	btn->text = text;
	btn->bw = 2;
	btn->type = type;
	btn->callback = f;
	btn->arg = arg;

	for (pp = &buttons; *pp; pp = &(*pp)->next)
		;
	*pp = btn;

	return (btn);
}

static void
draw_button(struct button *btn)
{
	XPoint pts[32], *p;
	GC gc = btn_gc;

	XSetForeground(dpy, gc, shades[btn->sunken ? 1 : 2]);

	XFillRectangle(dpy, area, gc, btn->x+btn->bw, btn->y+btn->bw,
		btn->w - 2*btn->bw, btn->h - 2*btn->bw);

	p = pts;
	p->x = btn->x + btn->bw;
	p->y = btn->y + btn->bw;
	p++;
	p->x = btn->w - btn->bw - btn->bw;
	p->y = 0;
	p++;
	p->x = btn->bw;
	p->y = -btn->bw;
	p++;
	p->x = -btn->w;
	p->y = 0;
	p++;
	p->x = 0;
	p->y = btn->h;
	p++;
	p->x = btn->bw;
	p->y = -btn->bw;
	p++;
	p->x = 0;
	p->y = -btn->h - btn->bw - btn->bw;
	p++;

	XSetForeground(dpy, gc, shades[btn->sunken ? 0 : 3]);
	XFillPolygon(dpy, area, gc, pts, p - pts, Complex, CoordModePrevious);

	p = pts;
	p->x = btn->x + btn->w - btn->bw;
	p->y = btn->y + btn->h - btn->bw;
	p++;
	p->x = 0;
	p->y = -btn->h + 2*btn->bw;
	p++;
	p->x = btn->bw;
	p->y = -btn->bw;
	p++;
	p->x = 0;
	p->y = btn->h;
	p++;
	p->x = -btn->w;
	p->y = 0;
	p++;
	p->x = btn->bw;
	p->y = -btn->bw;
	p++;
	p->x = btn->w;
	p->y = 0;
	p++;

	XSetForeground(dpy, gc, shades[btn->sunken ? 3 : 0]);
	XFillPolygon(dpy, area, gc, pts, p - pts, Complex, CoordModePrevious);

	XSetForeground(dpy, gc, black.pixel);
	XDrawString(dpy, area, gc,
		btn->x + btn->tx, btn->y + btn->ty,
		btn->text, strlen(btn->text));

	if (btn->type == TOGGLE) {
		p = pts;
		p->x = btn->x + btn->w - 2* btn->bw - 4;
		p->y = btn->y + 2 * btn->bw;
		p++;
		p->x = 4;
		p->y = 0;
		p++;
		p->x = 0;
		p->y = 4;
		p++;
		p->x = -4;
		p->y = 0;
		p++;
		p->x = 0;
		p->y = -4;
		p++;

		XSetForeground(dpy, gc, btn->active ? red.pixel : shades[0]);
		XFillPolygon(dpy, area, gc, pts, p - pts, Complex, CoordModePrevious);
	}
}

static void
paint_buttons()
{
	struct button *btn;

	for (btn = buttons; btn; btn = btn->next)
		draw_button(btn);
}

static int
push_button(XButtonEvent *eb)
{
	struct button *btn;

	if (eb->button == 1) {
		for (btn = buttons; btn; btn = btn->next)
			if (eb->x > btn->x && eb->x < btn->x + btn->w
			 && eb->y > btn->y && eb->y < btn->y + btn->h) {
				btn->sunken = True;
				if (btn->type == TOGGLE)
					btn->active ^= 1;
				else
					btn->active = True;
				draw_button(btn);
				if (btn->callback)
					(*btn->callback)(btn->arg, btn->active);
				return (True);
			}
	}
	return (False);
}

static int
release_button(XButtonEvent *eb)
{
	struct button *btn;

	if (eb->button == 1) {
		for (btn = buttons; btn; btn = btn->next)
			if (btn->sunken) {
				if (btn->type == INSTANT) {
					btn->active = False;
					if (btn->callback)
						(*btn->callback)(btn->arg, btn->active);
				}
				btn->sunken = False;
				draw_button(btn);
				return (True);
			}
	}
	return (False);
}

static int
pushing_button()
{
	struct button *btn;

	for (btn = buttons; btn; btn = btn->next)
		if (btn->sunken)
			return (True);
	return (False);
}

static void
init_button_stuff()
{
	XColor color;
	XGCValues gcv;
	int i;
	struct button *btn;
	char *fname = "-*-helvetica-bold-r-normal-*-12-*-*-*-*-*-iso8859-1";

	btn_font = XLoadQueryFont(dpy, fname);
	if (!btn_font) {
		fprintf(stderr, "%s missing, using main font.\n", fname);
		btn_font = font;
	}

	for (i = 0; i < 4; ++i) {
		color.red   = 0x7000 + i * 0x2000;
		color.green = 0x7000 + i * 0x2000;
		color.blue  = 0x7000 + i * 0x2000;

		if (!XAllocColor(dpy, DefaultColormap(dpy, screen), &color)) {
			fprintf(stderr, "Color problem.\n");
			exit(1);
		} else
			shades[i] = color.pixel;
	}

	gcv.function = GXcopy;
	gcv.font = btn_font->fid;
	gcv.plane_mask = AllPlanes;
	gcv.line_width = 0;
	gcv.graphics_exposures = False;
	gcv.background = shades[2];
	gcv.foreground = black.pixel;
	gcv.subwindow_mode = IncludeInferiors;
	btn_gc = XCreateGC(dpy, root,
		GCFunction | GCFont | GCLineWidth | GCPlaneMask |
		GCGraphicsExposures |
		GCBackground | GCForeground | GCSubwindowMode,
		&gcv);
}

static void
align_buttons()
{
	struct button *btn;
	XCharStruct overall;
	int dir, asc, desc;
	int w = 0, h = 0;
	int n, off, nbuttons = 0;
	int Asc = 0;

	for (btn = buttons; btn; btn = btn->next) {
		XTextExtents(btn_font, btn->text, strlen(btn->text),
			&dir, &asc, &desc, &overall);

		if (h < asc + desc)
			h = asc + desc;
		if (Asc < asc)
			Asc = asc;

		nbuttons++;

		btn->tw = overall.width;

		if (button_width < btn->bw * 2 + 3 * btn->tw / 2)
			button_width = btn->bw * 2 + 3 * btn->tw / 2;
	}

	/* XXX text crimped inside bewels ? use 'font' instead (into gc too) */

	off = (areaheight - areaheight / nbuttons * nbuttons) / 2;

	for (n = 0, btn = buttons; btn; btn = btn->next, ++n) {
		btn->w = button_width;
		btn->x = areawidth - button_width;
		btn->h = areaheight / nbuttons;
		btn->y = btn->h * n + off;

		btn->tx = btn->bw + (btn->w - 2 * btn->bw - btn->tw) / 2;
		btn->ty = btn->bw + btn->h / 2 - h / 2 + Asc - 1;
	}
}

static void
sync_buttons()
{
	if (button_halt->active != halted) {
		button_halt->active = halted;
		draw_button(button_halt);
	}
}

static void
init_buttons()
{
	init_button_stuff();

	new_button("Chan",  INSTANT, cb_chan, 0);
	new_button("Scale", INSTANT, cb_scale, 0);
	new_button("Fast",  INSTANT, cb_rate, +1);
	new_button("Slow",  INSTANT, cb_rate, -1);
	new_button("Next",  INSTANT, cb_step, +1);
	new_button("Prev",  INSTANT, cb_step, -1);
	button_halt =
	new_button("Halt",  TOGGLE,  cb_halt, 0);

	align_buttons();
}

