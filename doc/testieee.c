#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

static void
swapb(l, f, n)
char *l, *f;
int n;
{    register int i;

     for (i= 0; i< n; i++)
	f[i]= l[n-i-1];
}

/* Copyright (C) 1988-1991 Apple Computer, Inc.
 * All rights reserved.
 *
 * Machine-independent I/O routines for IEEE floating-point numbers.
 *
 * NaN's and infinities are converted to HUGE_VAL or HUGE, which
 * happens to be infinity on IEEE machines.  Unfortunately, it is
 * impossible to preserve NaN's in a machine-independent way.
 * Infinities are, however, preserved on IEEE machines.
 *
 * These routines have been tested on the following machines:
 *    Apple Macintosh, MPW 3.1 C compiler
 *    Apple Macintosh, THINK C compiler
 *    Silicon Graphics IRIS, MIPS compiler
 *    Cray X/MP and Y/MP
 *    Digital Equipment VAX
 *
 *
 * Implemented by Malcolm Slaney and Ken Turkowski.
 *
 * Malcolm Slaney contributions during 1988-1990 include big- and little-
 * endian file I/O, conversion to and from Motorola's extended 80-bit
 * floating-point format, and conversions to and from IEEE single-
 * precision floating-point format.
 *
 * In 1991, Ken Turkowski implemented the conversions to and from
 * IEEE double-precision format, added more precision to the extended
 * conversions, and accommodated conversions involving +/- infinity,
 * NaN's, and denormalized numbers.
 */

#ifndef HUGE_VAL
# define HUGE_VAL HUGE
#endif /*HUGE_VAL*/

#define ULONG u_int32_t
#define LONG int32_t

# define FloatToUnsigned(f)      ((ULONG)(((LONG)(f - 2147483648.0)) + 2147483647L) + 1)

void ConvertToIeeeExtended(num, bytes)
double num;
char *bytes;
{
    int    sign;
    int expon;
    double fMant, fsMant;
    ULONG hiMant, loMant;

    if (num < 0) {
        sign = 0x8000;
        num *= -1;
    } else {
        sign = 0;
    }

    if (num == 0) {
        expon = 0; hiMant = 0; loMant = 0;
    }
    else {
        fMant = frexp(num, &expon);
        if ((expon > 16384) || !(fMant < 1)) {    /* Infinity or NaN */
            expon = sign|0x7FFF; hiMant = 0; loMant = 0; /* infinity */
        }
        else {    /* Finite */
            expon += 16382;
            if (expon < 0) {    /* denormalized */
                fMant = ldexp(fMant, expon);
                expon = 0;
            }
            expon |= sign;
            fMant = ldexp(fMant, 32);          
            fsMant = floor(fMant); 
            hiMant = FloatToUnsigned(fsMant);
            fMant = ldexp(fMant - fsMant, 32); 
            fsMant = floor(fMant); 
            loMant = FloatToUnsigned(fsMant);
        }
    }
    
    bytes[0] = expon >> 8;
    bytes[1] = expon;
    bytes[2] = hiMant >> 24;
    bytes[3] = hiMant >> 16;
    bytes[4] = hiMant >> 8;
    bytes[5] = hiMant;
    bytes[6] = loMant >> 24;
    bytes[7] = loMant >> 16;
    bytes[8] = loMant >> 8;
    bytes[9] = loMant;
}

int main(int argc, char **argv) {

  double num = 44100.0;
  long long int numL;
  unsigned char numA[10];
  unsigned char tmp[8];
  if (argc == 2) num = strtod(argv[1], NULL);

  printf("num %g", num);
  memcpy(&numL, (void*) &num, 8);
  printf("numl %qd", numL);
/*   numL = numL << 8; */
  printf("numl %qd", numL);
  memcpy(&num, &numL, 8);
  printf("num %g", num);

  memcpy(numA, (void*) &num, 8);
  
/*   swapb(numA, tmp, 8); */
/*   memcpy(tmp, &num, 8); */

/*   ConvertToIeeeExtended(num, numA); */
  
  printf("sizeof short : %d\n", sizeof (short));
  printf("sizeof long : %d\n", sizeof (long));
  printf("sizeof long long int: %d\n", sizeof (long long int));
  printf("sizeof float : %d\n", sizeof (float));
  printf("sizeof double : %d\n", sizeof (double));
  printf("double value: %g,
hex:       %02x      %02x      %02x      %02x      %02x      %02x      %02x      %02x      %02x      %02x
bin: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n",
         num, numA[0], numA[1], numA[2], numA[3], numA[4], numA[5], numA[6], numA[7], numA[8], numA[9],
         !!(numA[0] & 0x01), !!(numA[0] & 0x02), !!(numA[0] & 0x04), !!(numA[0] & 0x08),
         !!(numA[0] & 0x10), !!(numA[0] & 0x20), !!(numA[0] & 0x40), !!(numA[0] & 0x80),
         !!(numA[1] & 0x01), !!(numA[1] & 0x02), !!(numA[1] & 0x04), !!(numA[1] & 0x08),
         !!(numA[1] & 0x10), !!(numA[1] & 0x20), !!(numA[1] & 0x40), !!(numA[1] & 0x80),
         !!(numA[2] & 0x01), !!(numA[2] & 0x02), !!(numA[2] & 0x04), !!(numA[2] & 0x08),
         !!(numA[2] & 0x10), !!(numA[2] & 0x20), !!(numA[2] & 0x40), !!(numA[2] & 0x80),
         !!(numA[3] & 0x01), !!(numA[3] & 0x02), !!(numA[3] & 0x04), !!(numA[3] & 0x08),
         !!(numA[3] & 0x10), !!(numA[3] & 0x20), !!(numA[3] & 0x40), !!(numA[3] & 0x80),
         !!(numA[4] & 0x01), !!(numA[4] & 0x02), !!(numA[4] & 0x04), !!(numA[4] & 0x08),
         !!(numA[4] & 0x10), !!(numA[4] & 0x20), !!(numA[4] & 0x40), !!(numA[4] & 0x80),
         !!(numA[5] & 0x01), !!(numA[5] & 0x02), !!(numA[5] & 0x04), !!(numA[5] & 0x08),
         !!(numA[5] & 0x10), !!(numA[5] & 0x20), !!(numA[5] & 0x40), !!(numA[5] & 0x80),
         !!(numA[6] & 0x01), !!(numA[6] & 0x02), !!(numA[6] & 0x04), !!(numA[6] & 0x08),
         !!(numA[6] & 0x10), !!(numA[6] & 0x20), !!(numA[6] & 0x40), !!(numA[6] & 0x80),
         !!(numA[7] & 0x01), !!(numA[7] & 0x02), !!(numA[7] & 0x04), !!(numA[7] & 0x08),
         !!(numA[7] & 0x10), !!(numA[7] & 0x20), !!(numA[7] & 0x40), !!(numA[7] & 0x80),
         !!(numA[8] & 0x01), !!(numA[8] & 0x02), !!(numA[8] & 0x04), !!(numA[8] & 0x08),
         !!(numA[8] & 0x10), !!(numA[8] & 0x20), !!(numA[8] & 0x40), !!(numA[8] & 0x80),
         !!(numA[9] & 0x01), !!(numA[9] & 0x02), !!(numA[9] & 0x04), !!(numA[9] & 0x08),
         !!(numA[9] & 0x10), !!(numA[9] & 0x20), !!(numA[9] & 0x40), !!(numA[9] & 0x80));

  return 0;
}
