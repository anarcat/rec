/****************************-*-C-*-***********************************
 * $Id: template.h,v 1.3 2001/08/22 21:24:02 anarcat Exp $
 **********************************************************************
 * Header file for rec
 **********************************************************************
 *   Copyright (C) 2001 The Anarcat <anarcat@anarcat.dyndns.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef REC_H
#define REC_H

typedef struct _dsp_parameters {

  char* device;                 /* device name */
  unsigned int rate;            /* read sample rate */
  unsigned int channels;        /* channels to use (1: mono, 2: stereo) */
  unsigned int fmt;             /* read sample format (8 bits or 16 bits) */
  
} dsp_parameters;

#endif
