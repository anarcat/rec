/****************************-*-C-*-***********************************
 * $Id: convert.h,v 1.4 2001/08/15 17:45:17 anarcat Exp anarcat $
 **********************************************************************
 * Conversion routines for sound parameters
 **********************************************************************
 *   Copyright (C) 2001 The Anarcat
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#ifndef CONVERT_H
#define CONVERT_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef USE_AIFF
/* Compute the "very large number" so that a maximum number of samples
 * can be transmitted through a pipe without the risk of causing
 * overflow when calculating the number of bytes.  At 48 kHz, 16 bits
 * stereo, this gives ~3 hours of music.  Sorry, the AIFF format does
 * not provide for an "infinite" number of samples. */
#define MAX_BYTES 0x7f000000L
#endif

#define MAX_FRAMES(size, channels) (MAX_BYTES / ((size)/8) * (channels))
#define MAX_TIME(size, channels, frames) \
(MAX_FRAMES(size, channels) / (frames))

#define BYTES_FROM_FRAMES(frames, size, channels) \
                         ((frames) * (channels) * ((size)/8))

#define SAMPLES_FROM_BYTES(bytes, size) \
                         ((bytes) / ((size) / 8))

#define FRAMES_FROM_BYTES(bytes, size, channels) \
                         (SAMPLES_FROM_BYTES(bytes, size) / (channels))

#define TIME_FROM_BYTES(bytes, size, channels, rate) \
                       (FRAMES_FROM_BYTES(bytes, size, channels) / (rate))

#define FRAMES_FROM_TIME(time, rate) \
                         ((time) * (rate))

#define SAMPLES_FROM_TIME(time, rate, channels) \
                         (FRAMES_FROM_TIME(time, rate) * (channels))

#define BYTES_FROM_TIME(time, rate, channels, size) \
                       (SAMPLES_FROM_TIME(time, rate, channels) * ((size)/8))

#endif
