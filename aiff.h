/***********************************************************************
 * AIFF writing implementation for FreeBSD
 ***********************************************************************
 * Pending (c)opysomething 26-04-2000 because of doubleToIEEEExtended()
 ***********************************************************************
 * $Id: aiff.h,v 1.11 2001/08/23 16:11:23 anarcat Exp anarcat $
 **********************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/**
 * Suggested use
 *
 * 1- Fill a AIFFSound struct with appropriate values.
 *
 *    - You can specify a value for ckDataSize in the SoundDataChunk
 *    struct if you wish to limit recording.
 *
 *    - A fd "ready" given in the SoundDataChunk will be read and copied
 *    to the file stream as output from a DSP device (/dev/dsp).
 *
 * 2- Call writeAIFFChunk() with an opened fd for output as argument.
 *
 *    - This will update the AIFF sound counters as the sound is
 *    "recorded".
 *
 *    - If you wish to handle interrupts, you will have to, in the
 *    interrupt handler:
 *
 *      a- call stopRecording() to set various counters to appropriate
 *      values and write a padding byte if necessary
 *
 *      b- call writeAIFFChunk() to write back these new values to file
 *
 * This only handles 8 and 16 bit sounds.
 *
 * This is designed for FreeBSD running on a Pentium. It hasn't been
 * tested on other platforms.
 **/

/*
 * Here we assume that:
 * typedef      __signed char              __int8_t;
 * typedef      unsigned char             __uint8_t;
 * typedef      short                     __int16_t;
 * typedef      unsigned short           __uint16_t;
 * typedef      int                       __int32_t;
 * typedef      unsigned int             __uint32_t;
 */

#ifdef PUBLIC_INTEREST
/* this is the model of a AIFF chunk */
typedef struct {                /* generic AIFF(-C) chunk */
  char ckID[4];                 /* chunk ID */
  int ckDataSize;               /* chunk data size, in bytes */
  char *ckData;                 /* data */
} Chunk;
#endif

#define CommonID "COMM"         /* ckID for Common Chunk */
typedef struct {                /* Common Chunk, describes basic sound
                                   info */
  char ckID[4];                 /* chunk ID is "COMM"  */
  int ckDataSize;               /* chunk size is always 18 */
  short numChannels;            /* # audio channels */
  unsigned int numSampleFrames; /* # sample frames = samplesCount/channelCount */
  short sampleSize;             /* # bits/sample */
  int sampleRate;               /* sample_frames/sec */
} CommonChunk;

#define SoundDataID "SSND"      /* ckID for Sound Data Chunk */
typedef struct {                /* Sound chunk contains sound data */
  char ckID[4];                 /* chunk ID is "SSND" */
  int ckDataSize;               /* chunk size defined as:
                                 * 8 + nframes * channels * size */
  /* where the first sample frame starts in bytes */
  unsigned int offset;
  /* used in conjunction with offset for block-aligning sound data.  It
   * contains the size in bytes of the blocks that sound data is aligned
   * to. */
  unsigned int blockSize;
  
  /* soundData contains the sample frames that make up the sound.  The
   * number of sample frames in the soundData is determined by the
   * numSampleFrames parameter in the Common Chunk.  If soundData[]
   * contains an odd number of bytes, a zero pad byte is added at the
   * end [but not used for playback].  Each sample point in a sample
   * frame is a linear, 2's complement value.  Sample points are from 1
   * to 32 bits wide, as determined by the sampleSize parameter in the
   * Common Chunk. */
  int fd;                       /* descriptor to raw PCM stream */
} SoundDataChunk;

#define FORMId "FORM"
#define AIFFId "AIFF"
typedef struct {                /* AIFF Sound */
  char ckID[4];                 /* chunk ID is "FORM" */
  int ckDataSize;               /* size of file */
  char formType[4];             /* file type is "AIFF"  */
  CommonChunk comm;             /* the COMM chunk, defined below */
  SoundDataChunk ssnd;          /* the SSND chunk, defined below */
} FormAIFFChunk;                /* in fact, a AIFF Sound is a FORM chunk */

typedef FormAIFFChunk AIFFSound; /* an AIFFSound is a FORM chunk */

/* "stops" recording and writes padding byte if necessary
 * calls updateSizeFields */
void stopRecording(int aiffSndFile, AIFFSound *snd);

/* update fields in struct by the ckDataSize of the SoundDataChunk
 * struct */
void updateSizeFields(AIFFSound *snd);

/* write the given 'Common' chunk to descriptor fd */
short writeCOMMChunk(int fd, CommonChunk *comm);

/**
 * write the given Sound chunk to descriptor fd
 *
 * Reads raw sound data from dspfd if valid ( > 0 ).
 *
 * Returns -1 if a problem occurred in a read operation from DSP
 **/
short writeSSNDChunk(int fd, AIFFSound* snd, int bufsize);

/**
 * Writes the given AIFF sound to the given stream.
 *
 * This calls writeSSNDChunk() and writeCOMMChunk(), so if dspfd >= 0,
 * raw (signed linear complement 2)sound data is read from dspfd.
 *
 * You can specify a buffer size, if 0, will take REC_BUFSIZE
 *
 * Returns the return value of writeSSNDChunk()
 **/
short writeAIFFSound(int fd, AIFFSound *snd, int bufsize);
