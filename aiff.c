/****************************-*-C-*-***********************************
 * $Id: aiff.c,v 1.15 2001/08/23 18:13:39 anarcat Exp anarcat $
 **********************************************************************
 * AIFF writing implementation for FreeBSD
 **********************************************************************
 *   Copyright (C) 2000 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 *********************************************************************/

#include <fcntl.h>
#include <signal.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <err.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef NO_DISPLAY
#include "display.h"
#endif
#include "aiff.h"
#include "convert.h"
#include "ieee_ext.h"

/* Byte swappers */

/* swap bytes of a 16 bits integer */
__inline__ unsigned short swapw(unsigned short us)
{return ((us >> 8) | (us << 8)) & 0xffff;}

/* swap bytes of a 32 bit integer */
__inline__ long swapl(long ul)
{return (ul >> 24) | ((ul >> 8) & 0xff00) /* left to right */
   | ((ul << 8) & 0xff0000L) | (ul << 24);} /* right to left */

/* "stops" recording and writes padding byte if necessary
 * calls updateSizeFields */
void stopRecording(int aiffSndFile, AIFFSound *snd)
{
  if (snd->ssnd.ckDataSize % 2 == 1) { /* odd number of bytes */
    lseek(aiffSndFile, 0, SEEK_END); /* goto EOF */
    write(aiffSndFile, "\0", 1); /* padding byte */
    fprintf(stderr, "Adding padding byte\n");
    snd->ckDataSize++;
  }
  
  close(snd->ssnd.fd);
  snd->ssnd.fd = -1;            /* mark it as closed for writeSSNDChunk */

  updateSizeFields(snd);
}

/* update fields in struct by the ckDataSize of the SoundDataChunk
 * struct */
void updateSizeFields(AIFFSound *snd)
{
  snd->comm.numSampleFrames =
    FRAMES_FROM_BYTES(snd->ssnd.ckDataSize - 8, snd->comm.sampleSize, snd->comm.numChannels);
  /* file size is:  FORM + COMM + SSND frame + SSND data + mysterious pad */
  snd->ckDataSize =  12  +  26  +   snd->ssnd.ckDataSize +       1;
}

short isReadyToRead(AIFFSound *snd)
{return snd->ssnd.fd < 0;}

void notReadyToRead(AIFFSound *snd)
{close(snd->ssnd.fd);snd->ssnd.fd = -1;}

/* write the given 'Common' chunk to descriptor fd */
short writeCOMMChunk(int fd, CommonChunk *comm)
{
  int tmp;                      /* two buffers for swaps */
  short tmp2;                   /* ... */
  char ieee[10];
  
  /* if (lseek(fd, 12, SEEK_SET) < 0) */ /* skip FORM chunk */
/*     return -1; */
    
  memcpy(comm->ckID, CommonID, 4); /* Commun Chunk ID is always "COMM" */
  comm->ckDataSize = 18;        /* chunk size is always 18 for COMM */
  if (write(fd, comm->ckID, 4) < 0) /* write common ID "COMM" */
    return -1;
    
  tmp = swapl(comm->ckDataSize); /* swap little endian bytes to big */
  if (write(fd, &tmp, 4) < 0)   /* write chunk size */
    return -1;
    
  tmp2 = swapw(comm->numChannels); /* swap little endian to big */
  if (write(fd, &tmp2, 2) < 0)  /* write channels count */
    return -1;
    
  tmp = swapl(comm->numSampleFrames); /* swap little endian to big */
  if (write(fd, &tmp, 4) < 0)   /* write the number of frames */
    return -1;
  
  tmp = swapw(comm->sampleSize); /* swap little endian to big */
  if (write(fd, &tmp, 2) < 0)   /* write the sample size in bits */
    return -1;
  
  doubleToIEEEExtended(comm->sampleRate, ieee);
  if (write(fd, ieee, 10) < 0) /* write sample rate */
    return -1;
  
  return 0;
} /* end writeCOMMChunk() */

/**
 * write the given Sound chunk to descriptor fd
 *
 * Reads raw sound data from dspfd if valid ( > 0 ).
 *
 * Returns -1 if a problem occurred in a read operation from DSP
 **/
short writeSSNDChunk(int fd, AIFFSound* snd, int bufsize)
{
  int swapped;
  SoundDataChunk *ssnd = &snd->ssnd;
  if (bufsize <= 0) {
#ifdef REC_BUFSIZE
    bufsize = REC_BUFSIZE;
#else
    bufsize = BUFSIZ;
#endif
  }
  /* skip FORM + COMM */
/*   if (lseek(fd, 12 + sizeof(CommonChunk) , SEEK_SET) < 0) */
/*     return -1; */
  
  memcpy(ssnd->ckID, SoundDataID, 4); /* chunk ID is always "SSND" */
  /* minimum chunk size is 18 */
  if (ssnd->ckDataSize < 8) ssnd->ckDataSize = 8;
  ssnd->offset = 0;             /* NOT USED */
  ssnd->blockSize = 0;          /* NOT USED */
  swapped = swapl(ssnd->ckDataSize); /* swap from big to little endian */
  
  if (
      (write(fd, ssnd->ckID, 4) < 0) /* write chunk ID, 4 bytes */
      || (write(fd, &swapped, 4) < 0) /* write chunk size, 4 bytes */
      || (write(fd, &ssnd->offset, 4) < 0) /* write offset, 4 bytes */
      || (write(fd, &ssnd->blockSize, 4) < 0)) { /* write blocksize, 4 bytes */
    perror("I/O error while writing SSND header");
    return -1;
  }
  
  /* FIX ME: */
  /* I don't know why, but data is garbled without this padding byte */
  if (write(fd, "0", 1) < 0)
    {
      perror("I/O error while writing SSND header");
      return -1;
    }
  
  if (ssnd->fd > 0) {           /* if sound stream opened */

    unsigned char *buffer = malloc(bufsize);
    int samplesToRead;          /* number of samples from DSP */
    int readCount = 0;          /* counter, buffer to swap */
  
    samplesToRead = ssnd->ckDataSize; /* get the number of samples to read */

    ssnd->ckDataSize = 8;       /* no bytes written now */
    while (ssnd->ckDataSize < samplesToRead) { /* read long s */
      readCount = bufsize;       /* read bufsize bytes */
      /* last chunk smaller than buffer.
       * should we use a seperate counter * for (samplesToRead -
       * ssnd->ckDataSize) ? */
      if ( readCount > (samplesToRead - ssnd->ckDataSize))
        readCount = (samplesToRead - ssnd->ckDataSize);
      readCount = read(ssnd->fd, buffer, readCount); /* read from DSP */
      ssnd->ckDataSize += readCount; /* cout read bytes */
      if (readCount < 0) {
        perror("IO error while reading from DSP");
        return -1;
      }

      if (write(fd, buffer, readCount) < 0) { /* write buffer to file */
        perror("IO error while writing file");
        return -1;
      }

#ifndef NO_DISPLAY
      update_display(readCount, 
                     TIME_FROM_BYTES
                     ((float) readCount, snd->comm.sampleSize,
                      snd->comm.numChannels, snd->comm.sampleRate));
#endif

    } /* end transfer loop */
  } /* end write of sound */
  
  return 0;
  
} /* end writeSSNDChunk() */

/**
 * Writes the given AIFF sound to the given stream.
 *
 * This calls writeSSNDChunk() and writeCOMMChunk(), so if dspfd >= 0,
 * raw (signed linear complement 2)sound data is read from dspfd.
 *
 * Returns the return value of writeSSNDChunk()
 **/
short writeAIFFSound(int fd, AIFFSound *sound, int bufsize)
{
  int swapped = swapl(sound->ckDataSize); /* swap 16 bits int */

  lseek(fd, SEEK_SET, 0);       /* beginning of file */

  memcpy(sound->ckID, FORMId, 4); /* chunk name is "FORM" */
  memcpy(sound->formType, AIFFId, 4); /* format is "AIFF" */
  
  if (
      (write(fd, sound->ckID, 4) < 0) /* write chunk name */
      || (write(fd, &swapped, 4) < 0) /* write data size */
      || (write(fd, sound->formType, 4) < 0) /* write format */
      ) {
    perror("I/O error writing FORM tag");
    return -1;
  }

  if (writeCOMMChunk(fd, &sound->comm) < 0) { /* write "COMM" chunk  */
    perror("IO error while writing COMM chunk");
    return -1;                  /* return if error */
  }

  
  swapped = writeSSNDChunk(fd, sound, bufsize); /* write "SSND" chunk: sound data */

  /* we can still write over what we made in the COMM chunk if
   * writeSSNDChunk() returns with an error */
  if (sound->ssnd.fd > 0) updateSizeFields(sound);

  return swapped;
  
} /* end writeAIFFSound() */
